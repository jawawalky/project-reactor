/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.reactor.operators;

import demo.reactor.util.Demo;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * In this demo you will learn how to create Mono and Flux objects and
 * how to consume the values that they produce.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		this.monoWithResult();
		this.monoWithError();
		this.monoWithConsumer();
		this.monoWithSubscriber();
		this.flux();

	}

	/**
	 * In this demo we observe, how a {@code Mono} flow is created and how we
	 * can consume its value in a synchronous call to the blocking method
	 * {@code Mono#block()}. Note that normally blocking calls should be
	 * avoided. Non-blocking examples will follow.
	 */
	private void monoWithResult() {

		Demo.log("A) Mono with regular result ...");

		var source = Mono.just("Hello!");        // <- We create the only
		                                              //    value emitted by
		                                              //    Mono here.

		var result = source.block();                  // <- We wait for
		                                              //    for the result of
		                                              //    the Mono.
		                                              //    (Blocking code!)

		Demo.log("  Result: " + result);

	}

	/**
	 * This time our {@code Mono} produces an error. The {@code Mono#block()}
	 * method will throw the causing exception.
	 */
	private void monoWithError() {

		Demo.log("B) Mono with error ...");

		try {

			var source = Mono.error(new Exception("Something went wrong!"));
			source.block();            // <- Blocking method notifies about
			                           //    an error by throwing
			                           //    an exception.

		} // try
		catch (Exception e) {

			Demo.log("  Error: " + e.getMessage());

		} // catch

	}

	/**
	 * Now we use a callback function to process the outcome of
	 * a {@code Mono}.
	 */
	private void monoWithConsumer() {

		Demo.log("C) Mono with consumer ...");

		var source1 = Mono.just("Hello!");
		source1.subscribe(v -> Demo.log("  1) Result: " + v));

        var source2 = Mono.error(new Exception("Something went wrong!"));
        source2.subscribe(
			v -> Demo.log("  2) Result: " + v),
			e -> Demo.log("  2) Fault: " + e.getMessage())
		);

		var source3 = Mono.just("Hello!");
		source3.subscribe(
			v -> Demo.log("  3) Result: " + v),
			e -> Demo.log("  3) Fault: " + e.getMessage()),
			() -> Demo.log("  3) Finished.")
		);

		var source4 = Mono.error(new Exception("Something went wrong!"));
		source4.subscribe(
			v -> Demo.log("  4) Result: " + v),
			e -> Demo.log("  4) Fault: " + e.getMessage()),
			() -> Demo.log("  4) Finished.")
		);

	}

	/**
	 * Now we use a {@code Subscriber} to process the outcome of
	 * a {@code Mono}.
	 */
	private void monoWithSubscriber() {

		Demo.log("D) Mono with subscriber ...");

		var source = Mono.just("Hello!");
		source.subscribe(new Subscriber<>() {

			private Subscription subscription;

			@Override
			public void onSubscribe(Subscription subscription) {

				this.subscription = subscription;                              // <- The subscription allows us to
				                                                               //    cancel the connection to
				                                                               //    the publisher and to signal
				                                                               //    the capacity of  the subscriber
				                                                               //    (= backpressure).

				this.subscription.request(1);                               // <- Tells the publisher how many
				                                                               //    values the subscriber is capable
				                                                               //    to process.

			}

			@Override
			public void onNext(String value) {                                 // <- Called each time the publisher has
				                                                               //    a new value.

				Demo.log("  Result: " + value);
				this.subscription.request(1);                                  // <- Signal the publisher that
				                                                               //    the subscriber can process
				                                                               //    another value.

			}

			@Override
			public void onError(Throwable cause) {                             // <- Called, when an error
				                                                               //    has occurred.

				Demo.log("  Fault: " + cause.getMessage());

			}

			@Override
			public void onComplete() {                                         // <- Called, when the flow is closed.

				Demo.log("  Finished.");

			}

		});

	}

	/**
	 * Now we use a {@code Flux} as {@code Publisher}. This allows us to
	 * publish more than one value. Apart from that the usage of a {@code Flux}
	 * is very similar to the usage of a {@code Mono}.
	 */
	private void flux() {

		Demo.log("E) Flux ...");

		Demo.log("  Create a flux and print its values:");
		var source1 = Flux.just("1", "2", "3");                                // <- Creates a flow with
		                                                                             //    the values "1", "2" and "3".
		source1.subscribe(v -> Demo.log("  " + v));

		Demo.log("  Create a flux from a range:");
		var source2 = Flux.range(4, 3);                                        // <- Create a flux with numbers
		                                                                       //    from a range.
		source2.subscribe(v -> Demo.log("  " + v));

		Demo.log("  Subscribe to a flow a second time:");
		source1.subscribe(v -> Demo.log("  " + v));                            // <- We can subscribe to flows
		                                                                       //    more than once.

	}

	/**
	 * Runs the demo application.
	 *
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
