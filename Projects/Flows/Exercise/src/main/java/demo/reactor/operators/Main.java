/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.reactor.operators;

import demo.reactor.util.Demo;
import reactor.core.publisher.Mono;

/**
 * In this demo you will learn how to create Mono and Flux objects and
 * how to consume the values that they produce.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		this.monoWithResult();
		this.monoWithError();
		this.monoWithConsumer();
		this.monoWithSubscriber();
		this.flux();

	}

	/**
	 * In this demo we observe, how a {@code Mono} flow is created and how we
	 * can consume its value in a synchronous call to the blocking method
	 * {@code Mono#block()}. Note that normally blocking calls should be
	 * avoided. Non-blocking examples will follow.
	 */
	private void monoWithResult() {

		Demo.log("A) Mono with regular result ...");

		// TODO
		//
		//  o Create a flow with the value "Hello!".
		//
		//  o Read the value from the flow with a blocking method and print
		//    it on the console.

	}

	/**
	 * This time our {@code Mono} produces an error. The {@code Mono#block()}
	 * method will throw the causing exception.
	 */
	private void monoWithError() {

		Demo.log("B) Mono with error ...");

		// TODO
		//
		//  o Create a flow, which fails with an exception.
		//
		//  o Try to read from the flow and process the exception.

	}

	/**
	 * Now we use a callback function to process the outcome of
	 * a {@code Mono}.
	 */
	private void monoWithConsumer() {

		Demo.log("C) Mono with consumer ...");

		// TODO
		//
		//  o Test the different variants of the 'subscribe(...)' that have
		//    consumers and actions as arguments.

	}

	/**
	 * Now we use a {@code Subscriber} to process the outcome of
	 * a {@code Mono}.
	 */
	private void monoWithSubscriber() {

		Demo.log("D) Mono with subscriber ...");

		var source = Mono.just("Hello!");

		// TODO
		//
		//  o Implement a 'Subscriber' that you can subscribe to the flow.

	}

	/**
	 * Now we use a {@code Flux} as {@code Publisher}. This allows us to
	 * publish more than one value. Apart from that the usage of a {@code Flux}
	 * is very similar to the usage of a {@code Mono}.
	 */
	private void flux() {

		Demo.log("E) Flux ...");

		// TODO
		//
		//  o Create a flow with the values "1", "2" and "3" and subscribe to it.
		//
		//  o Create a range flow with the number 4 - 6 and subscribe to it.
		//
		//  o Subscribe a second time to the first flow.

	}

	/**
	 * Runs the demo application.
	 *
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {

		new Main().runDemo();

	}

}
