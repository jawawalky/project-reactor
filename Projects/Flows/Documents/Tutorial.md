# Flows

The *Project Reactor* provides two core `Publisher` classes, `Mono` and
`Flux`. While `Mono` is designed for flows with *0|1* element, `Flux` is
used for flows with *0|N* elements.

```mermaid
classDiagram
    Publisher <|-- CorePublisher
    CorePublisher <|-- Mono
    CorePublisher <|-- Flux
    Publisher : +subscribe(Subscriber)
    class CorePublisher {
        +subscribe(CoreSubscriber)
    }
    class Mono {
        +just(T)
        +empty()
        +error(Throwable)
    }
    class Flux {
        +just(T...)
        +empty()
        +error(Throwable)
    }
```

Both flow classes provide a wide range of operators that can be applied
to the flow.

## Mono
The `Mono` class offers many static methods for the creation of a `Mono`.
We will present some of them.

### Creating a Flow with one Element
If we need a flow with exactly one value, then we can use `Mono.just(T)`.
We can consume its value with the blocking method `block()`, which returns
that value from the flow, when it is available.

:::note

It is the easiest way to consume the flow value by `block()`, but **not**
the preferred way, since it blocks the thread, which is, what we actually
want to avoid, because we would like to write non-blocking code.
In due course we will learn how to process the value in a non-blocking way.

:::

**Example**

```java
var flow = Mono.just("Hello!");        // (1)
var value = flow.block();              // (2)
System.out.println("Value: " + value);
```

1. Create a `Mono` with the value `Hello!`.
2. Wait for the result of the flow. (Blocking)

### Creating a Flow with an Error
Of course the result of a `Mono` can also be an error. We can notify about
an error by using `Mono.error(Throwable)`.

**Example**

```java title="Code that might cause an error"
public Mono getValue() {
    try {
        var value = readValueFromDatabase();
        return Mono.just(value);
    }
    catch (DBException e) {
        return Mono.error(e);
    }
}
```

```java title="Code that calls method"
public void processValue() {
    try {
        var value = getValue().block();
        // Do processing ...
    }
    catch (DBException e) {
        System.out.println("DB fault: " + e.getMessage());
    }
}
```

### Consuming a Flow with a Consumer
Instead of using a blocking method to get the values of a flow, we can use
a `Consumer` to process them. The consumer is called each time a new value
is availble from the flow. We can register a consumer with the method
`Mono.subscribe(Consumer)`.

**Example**

```java
var flow = Mono.just("Hello!");
flow.subscribe(v -> System.out.println(v));
```

:::note

The `Consumer` interface is a functional interface, so we can use a lambda
expression (`v -> System.out.println(v)`) to implement the consumer object.

:::

This code will produce the following output

```console
Hello!
```

### Consuming an Error with a Consumer
If we want to consume an error, we can that with the following code

**Example**

```java
var flow = Mono.error(new Exception("Something went wrong!"));
flow.subscribe(
    v -> System.out.println(v),
    e -> System.out.println(e.getMessage())
);
```

The second version of `Mono.subscribe(Consumer, Consumer)` has two `Consumer`
paramneters. The first consunmer (`v -> System.out.println(v)`) is for regular
values emitted by the flow and the second consumer
(`e -> System.out.println(e.getMessage())`) is for errors.

This code prints

```console
java.lang.Exception: Something went wrong!
```

on the console.

### Termination of a Flow
A flow either ends by some end signal or by an error. Both events terminate
a flow. We just saw, how we are informed about an error. If the flow ends
normally, i.e. it will not emit anymore values, then it calls a callback
function that signals the end of the flow.

**Example**

```java
var flow = Mono.just("Hello!");
flow.subscribe(
    v -> System.out.println(v),
    e -> System.out.println(e.getMessage()),
    () -> System.out.println("Finished.")
);
```

The last callback function (`() -> System.out.println("Finished.")`) is called,
when the flow is *complete*.

This code would print

```console
Hello!
Finished.
```

on the console.

The same code with an error

```java
var flow = Mono.error(new Exception("Something went wrong!"));
flow.subscribe(
    v -> System.out.println(v),
    e -> System.out.println(e.getMessage()),
    () -> System.out.println("Finished.")
);
```

produces only

```console
java.lang.Exception: Something went wrong!
```

So a flow terminates either normally and the *complete* callback will be
called or it ends exceptionally and the *error* callback will be called.
In both scenarios the flow will not emit anymore values.

### Subscriber

A `Mono` object is a `Publisher`. Publisher objects emit values that are
consumed by `Subscriber` objects. The structure of a subscriber looks like
this

```java
public interface Subscriber<T> {
    public void onSubscribe(Subscription s);
    public void onNext(T t);
    public void onError(Throwable t);
    public void onComplete();
}
```

The four methods of a `Subscriber` represent lifecycle event of the subscriber.

`onSubscribe(Subscription)` is called, when the `Subscriber` subscribes to
the `Publisher`. The `Publisher` passes a `Subscription` object to
the `onSubscribe(Subscription)` method of the `Subscriber`. Normally
the `Subscriber` internally saves the `Subscription` object, because it can
cancel the subscription through the `Subscription` object
(`Subscription.cancel()`) and it can signal to the publisher, how many more
values it can process (`Subscription.request(int)`).

```java
public interface Subscription {
    public void request(long n);
    public void cancel();
}
```

The `onNext(T)` method is called each time the `Publisher` publishes
a new value.

`onError(Throwable)` is called, when an error has occurred.

`onComplete()` is called, after the last value has been emitted by
the `Publisher`.

The `Mono.subscribe()` method is overloaded

```java
public final Disposable subscribe(Consumer<? super T> consumer) { ... }

public final Disposable subscribe(
    @Nullable Consumer<? super T> consumer,
    Consumer<? super Throwable>   errorConsumer
) { ... }

public final Disposable subscribe(
    @Nullable Consumer<? super T>         consumer,
    @Nullable Consumer<? super Throwable> errorConsumer,
    @Nullable Runnable                    completeConsumer
) { ... }

public final Disposable subscribe(
    @Nullable Consumer<? super T>            consumer,
    @Nullable Consumer<? super Throwable>    errorConsumer,
    @Nullable Runnable                       completeConsumer,
    @Nullable Consumer<? super Subscription> subscriptionConsumer
) { ... }

public final void subscribe(Subscriber<? super T> actual) { ... }
```

The first four methods are only convenience methods, so we do not have to
implement a new `Subscriber` each time we want to process a flow. Since
a `Consumer` does not have access to the `Subscription`, those variants of
the `subscribe()` method return a `Disposable` object, which allows us to
cancel the subscription (`Disposable.dispose()`).

```java
public interface Disposable {
    void dispose();
    default boolean isDisposed() { return false; }
}
```

## Flux

If we want to create flows with more than one value, then we use the class
`Flux`. `Flux` offers similar methods like those in the class `Mono` to
create flows.

**Example**

```java
var flow = Flux.just("1", "2", "3");
flow.subscribe(v -> System.out.println(v));
```

The way we consume the values of a `Flux` is identical to the way we consume
the value of a `Mono`. We can subscibe with a `Subscriber` or use the variants
that take consumers as arguments.

# Links
