/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.reactor.operators;

import demo.reactor.util.Demo;
import reactor.core.publisher.Flux;

import java.time.Duration;

/**
 * In this demo you will learn about flow operators.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() {

		this.map();
		this.flatMap();
		this.switchMap();

	}

	/**
	 * Emits the specified values in constant intervals of 500 milliseconds.
	 *
	 * @param values
	 *        the values to be emitted.
	 *
	 * @return A {@code Flux} of numbers.
	 */
	private Flux<Integer> emitValues(final int... values) {

		return
			Flux.interval(Duration.ofMillis(500L))
				.map(n -> values[n.intValue()])
				.take(values.length);

	}

	/**
	 * Emits <i>n</i> times the value <i>n</i> with an interval of
	 * 250 milliseconds.
	 *
	 * @param n
	 *        the value to be emitted n times.
	 *
	 * @return A {@code Flux} of numbers.
	 */
	private Flux<Integer> emitValueNtimes(final int n) {

		return
			Flux.interval(Duration.ofMillis(250L))
				.map(i -> n)
				.take(n);

	}

	/**
	 * The following happens
	 * <ul>
	 *     <li>
	 *         The values 1, 4 and 3 are emitted with an interval of
	 *         500 milliseconds.
	 *     </li>
	 *     <li>
	 *         Each value is multiplied by 2 and then 1 is added.
	 *     </li>
	 *     <li>
	 *         The numbers are converted to strings.
	 *     </li>
	 *     <li>
	 *         The values are printed on the console.
	 *     </li>
	 * </ul>
	 */
	private void map() {

		Demo.log("A) map() ...");

		this.emitValues(1, 4, 3)
			.map(n -> 2 * n + 1)
			.map(String::valueOf)
			.subscribe(Demo::println);

		Demo.sleep(2000L);

		Demo.println();

	}

	/**
	 * The following happens
	 * <ul>
	 *     <li>
	 *         The values 1, 4 and 3 are emitted with an interval of
	 *         500 milliseconds.
	 *     </li>
	 *     <li>
	 *         Each value is emitted <i>n</i> times with an interval
	 *         of 250 milliseconds parallel to other emitted values.
	 *         ({@code flatMap()})
	 *     </li>
	 *     <li>
	 *         The numbers are converted to strings.
	 *     </li>
	 *     <li>
	 *         The values are printed on the console.
	 *     </li>
	 * </ul>
	 */
	private void flatMap() {

		Demo.log("B) flatMap() ...");

		this.emitValues(1, 4, 3)
			.flatMap(this::emitValueNtimes)
			.map(String::valueOf)
			.subscribe(Demo::println);

		Demo.sleep(3000L);

		Demo.println();

	}

	/**
	 * The following happens
	 * <ul>
	 *     <li>
	 *         The values 1, 4 and 3 are emitted with an interval of
	 *         500 milliseconds.
	 *     </li>
	 *     <li>
	 *         Each value is emitted <i>n</i> times with an interval
	 *         of 250 milliseconds replacing other value emissions.
	 *         ({@code switchMap()})
	 *     </li>
	 *     <li>
	 *         The numbers are converted to strings.
	 *     </li>
	 *     <li>
	 *         The values are printed on the console.
	 *     </li>
	 * </ul>
	 */
	private void switchMap() {

		Demo.log("C) switchMap() ...");

		this.emitValues(1, 4, 3)
			.switchMap(this::emitValueNtimes)
			.map(String::valueOf)
			.subscribe(Demo::println);

		Demo.sleep(3000L);

		Demo.println();

	}

	/**
	 * Runs the demo application.
	 *
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) throws InterruptedException {

		new Main().runDemo();

	}

}
