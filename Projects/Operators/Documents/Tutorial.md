# Schedulers

## Threading in Flows

The *Reactor* framework and reactive programming in general are
*concurrency-agnostic*, i.e. they do not do multi-threading automatically,
nor do they impose a certain concurrency model.

So creating a flow does *not* mean, we are automatically performing it on
another thread. A flow operation is simply carried out on the same thread
as its ancestor operation, unless we explicitly change the thread.

## ExecutorService vs Scheduler

Concurrency is provided in *Java* by `Thread` objects. We can create threads
directly by instantiating `Thread` objects.

**Example**

```java
final Thread thread = new Thread(() -> System.out.println("Hello!"));
thread.start();
```

Always creating a new *platform* thread and disposing it after work,
is expensive, and platform threads are a limited resource. That's why often
*thread pools* are used. A thread pool contains a set of running `Thread`
objects. When we need a `Thread`, then we borrow it from the thread pool.
That makes using threads much more efficient and with a significant smaller
foot-print.

In the *JDK* thread pools and other thread suppliers are provided as

- `Executor` objects or more general as
- `ExecutorService` objects.

There is a utility class `Executors` with many static methods, providing
access to common thread suppliers and thread pools.

*Reactor* does not use `ExecutorService` directly, but provides a further
abstraction called `Scheduler`.

```java
public interface Scheduler extends Disposable {

    Disposable schedule(Runnable task);
    
    default Disposable schedule(Runnable task, long delay, TimeUnit unit) { ... }

    default Disposable schedulePeriodically(Runnable task, long initialDelay, long period, TimeUnit unit) { ... }

    default void dispose() { ... }

    // ...

}
```

A `Scheduler` can be used to run tasks (`Runnable`). These tasks can be
one-time tasks or periodic tasks.

There is also a utility class `Schedulers`, which offers static factory methods
for various `Scheduler` implementations. Some of the most fequently used
schedulers are

| Scheduler | Description | Comment |
| ----- | --------------- | -------------- |
| `Schedulers.immediate()` | Runs tasks immediately on the calling thread. | |
| `Schedulers.single()` | Runs all tasks on the same thread until disposed. | |
| `Schedulers.newSingle()` | Runs each task in a separate thread. | |
| `Schedulers.elastic()` | Unbound thread pool. | Better use `Schedulers.boundedElastic()`, since too many threads may cause pressure on the system and hide backpressure problems. |
| `Schedulers.boundedElastic()` | Bound thread pool. | Can be used with `ExecutorService` or since JDK 21 with *Virtual Threads*. |
| `Schedulers.parallel()` | A fixed pool of workers. | One worker per CPU core. |
| `Schedulers.fromExecutor(Executor)` | A `Scheduler` based on an `Executor`. | |
| `Schedulers.fromExecutorService(ExecutorService)` | A `Scheduler` based on an `ExecutorService`. | |

## Publish on Scheduler

Processing a flow is like working on an assembly line. Perhaps one worker is
doing the first steps and then another worker takes over to complete the next
steps. That is exactly, what the `publishOn(Scheduler)` method is doing.
Processing of our flow normally starts on the thread, where the `Subscriber`
subscribed to the flow. It we want to switch processing to another thread,
then we use `publishOn(Scheduler)`.

**Example**

```java
final Scheduler scheduler = Schedulers.newSingle("A"); 

final Flux<String> flow = Flux         // Runs on
    .range(1, 10)                      // <- 'thread:1'
    .map(v -> 2 * v)                   // <- 'thread:1'
    .publishOn(scheduler)              // <- 'A'
    .map(v -> "Value: " + v);          // <- 'A'

new Thread(() -> flux.subscribe(System.out::println)).start();
```

We can switch the scheduler as often as we like on the assembly line.

```java
final Scheduler schedulerA = Schedulers.newSingle("A"); 
final Scheduler schedulerB = Schedulers.newSingle("B"); 

final Flux<String> flow = Flux         // Runs on
    .range(1, 10)                      // <- 'thread:1'
    .map(v -> 2 * v)                   // <- 'thread:1'
    .publishOn(schedulerA)             // <- 'A'
    .map(v -> v + 1)                   // <- 'A'
    .publishOn(schedulerB)             // <- 'B'
    .map(v -> "Value: " + v);          // <- 'B'

new Thread(() -> flux.subscribe(System.out::println)).start();
```

## Subscribe on Scheduler

If we want the entire flow to run by a certain scheduler, then we can use
the `subscribeOn(Scheduler)` method.

```java
final Scheduler scheduler = Schedulers.newSingle("A"); 

final Flux<String> flow = Flux         // Runs on
    .range(1, 10)                      // <- 'A'
    .subscribeOn(scheduler)            // <- 'A'
    .map(v -> 2 * v)                   // <- 'A'
    .map(v -> "Value: " + v);          // <- 'A'

new Thread(() -> flux.subscribe(System.out::println)).start();
```

Usually we call it at the beginning of the assembly line, but that is not
a requirement.

So

```java
final Scheduler scheduler = Schedulers.newSingle("A"); 

final Flux<String> flow = Flux         // Runs on
    .range(1, 10)                      // <- 'A'
    .map(v -> 2 * v)                   // <- 'A'
    .map(v -> "Value: " + v)           // <- 'A'
    .subscribeOn(scheduler);           // <- 'A'

new Thread(() -> flux.subscribe(System.out::println)).start();
```

shows the same behavior.

# Links
