/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.reactor.operators;

import demo.reactor.util.Demo;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

/**
 * In this demo you will learn how to use schedulers.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() throws InterruptedException {

		this.runningInCallingThread();
		this.runningInSeparateThread();
		this.publishOnOperator();
		this.subscribeOnOperator();
		this.subscribeOnAndPublishOn();

	}

	/**
	 * Running a flow in the thread, where the subscription happened.
	 */
	private void runningInCallingThread() {

		Demo.logWithThread("A) Running in calling thread ...");

		Mono.just("Hello!").subscribe(Demo::logWithThread);

	}

	/**
	 * Running a flow in a separate thread.
	 */
	private void runningInSeparateThread() throws InterruptedException {

		Demo.logWithThread("B) Running in separate thread ...");

		final Thread thread = new Thread(
			() -> Mono.just("Hello!").subscribe(Demo::logWithThread)
		);
		thread.start();
		thread.join();

	}

	/**
	 * Using the 'publishOn()' operator.
	 */
	private void publishOnOperator() {

		Demo.logWithThread("C) Using the 'publishOn()' operator ...");

		final Scheduler schedulerA = Schedulers.newSingle("A");
		final Scheduler schedulerB = Schedulers.newSingle("B");

		Mono.just("Hello!")
			.doOnNext(Demo::logWithThread)
			.publishOn(schedulerA)
			.doOnNext(Demo::logWithThread)
			.publishOn(schedulerB)
			.doOnNext(Demo::logWithThread)
			.doFinally(s -> schedulerA.dispose())
			.doFinally(s -> schedulerB.dispose())
			.subscribe(Demo::logWithThread);

		Demo.sleep(20);

	}

	/**
	 * Using the 'subscribeOn()' operator.
	 */
	private void subscribeOnOperator() {

		Demo.logWithThread("D) Using the 'subscribeOn()' operator ...");

		final Scheduler scheduler = Schedulers.single();

		Mono.just("Hello!")
				.doOnNext(Demo::logWithThread)
				.subscribeOn(scheduler)
				.doOnNext(Demo::logWithThread)
				.doFinally(s -> scheduler.dispose())
				.subscribe(Demo::logWithThread);

		Demo.sleep(20);

	}

	/**
	 * Using the 'publishOn()' operator.
	 */
	private void subscribeOnAndPublishOn() {

		Demo.logWithThread("E) Using the 'subscribeOn()' and 'publishOn()' operator ...");

		final Scheduler schedulerA = Schedulers.newSingle("A");
		final Scheduler schedulerB = Schedulers.newSingle("B");
		final Scheduler schedulerC = Schedulers.newSingle("C");

		Mono.just("Hello!")
			.doOnNext(Demo::logWithThread)
			.publishOn(schedulerA)
			.doOnNext(Demo::logWithThread)
			.publishOn(schedulerB)
			.doOnNext(Demo::logWithThread)
			.subscribeOn(schedulerC)
			.doFinally(s -> schedulerA.dispose())
			.doFinally(s -> schedulerB.dispose())
			.doFinally(s -> schedulerC.dispose())
			.subscribe(Demo::logWithThread);

		Demo.sleep(20);

	}

	/**
	 * Runs the demo application.
	 *
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) throws InterruptedException {

		new Main().runDemo();

	}

}
