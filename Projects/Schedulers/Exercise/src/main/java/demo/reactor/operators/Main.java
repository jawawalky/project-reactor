/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2024 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.reactor.operators;

import demo.reactor.util.Demo;
import reactor.core.publisher.Mono;

/**
 * In this demo you will learn how to use schedulers.
 * 
 * @author Franz Tost
 *
 */
public class Main {

	// constructors /////

	private Main() { }
	

	// methods /////

	private void runDemo() throws InterruptedException {

		this.runningInCallingThread();
		this.runningInSeparateThread();
		this.publishOnOperator();
		this.subscribeOnOperator();
		this.subscribeOnAndPublishOn();

	}

	/**
	 * Running a flow in the thread, where the subscription happened.
	 */
	private void runningInCallingThread() {

		Demo.logWithThread("A) Running in calling thread ...");

		// TODO
		//
		//  o Create a flow with one element, "Hello!".
		//
		//  o Consume the flow and print the element with thread information
		//    (Hint: Demo.logWithThread()).

	}

	/**
	 * Running a flow in a separate thread.
	 */
	private void runningInSeparateThread() throws InterruptedException {

		Demo.logWithThread("B) Running in separate thread ...");

		// TODO
		//
		//  o Create a flow with one element, "Hello!".
		//
		//  o Subscribe to the flow in a different thread and print the element
		//    with thread information.
		//
		//  o Wait for the thread to complete.

	}

	/**
	 * Using the 'publishOn()' operator.
	 */
	private void publishOnOperator() {

		Demo.logWithThread("C) Using the 'publishOn()' operator ...");

		// TODO
		//
		//  o Create two schedulers 'A' and 'B'.
		//
		//  o Create a flow with one element "Hello!".
		//
		//  o Display the thread, on which the subscriber subscribed to
		//    the flow. (Hint: use an appropriate 'doOnXXX(...)' method.
		//
		//  o Switch the processing to scheduler 'A'.
		//
		//  o Display the thread, on which the subscriber subscribed to
		//    the flow.
		//
		//  o Switch the processing to scheduler 'B'.
		//
		//  o Display the thread, on which the subscriber subscribed to
		//    the flow.
		//
		//  o Dispose schedulers.
		//
		//  o Subscribe to the flow.

		Mono.just("Hello!");

		Demo.sleep(20);

	}

	/**
	 * Using the 'subscribeOn()' operator.
	 */
	private void subscribeOnOperator() {

		Demo.logWithThread("D) Using the 'subscribeOn()' operator ...");

		// TODO
		//
		//  o Create a scheduler.
		//
		//  o Create a flow with one element "Hello!".
		//
		//  o Display the thread, on which the subscriber subscribed to
		//    the flow.
		//
		//  o Switch the processing for the entire flow to that scheduler.
		//
		//  o Display the thread, on which the subscriber subscribed to
		//    the flow.
		//
		//  o Dispose scheduler.
		//
		//  o Subscribe to the flow.

		Mono.just("Hello!");

		Demo.sleep(20);

	}

	/**
	 * Using the 'publishOn()' operator.
	 */
	private void subscribeOnAndPublishOn() {

		Demo.logWithThread("E) Using the 'subscribeOn()' and 'publishOn()' operator ...");

		// TODO
		//
		//  o Create three schedulers.
		//
		//  o Use 'subscribeOn()' and 'publishOn()' in the flow, where you
		//    think it is appropriate.
		//
		//  o Dispose schedulers.

		Mono.just("Hello!")
			.doOnNext(Demo::logWithThread)
			.doOnNext(Demo::logWithThread)
			.doOnNext(Demo::logWithThread)
			.subscribe(Demo::logWithThread);

		Demo.sleep(20);

	}

	/**
	 * Runs the demo application.
	 *
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) throws InterruptedException {

		new Main().runDemo();

	}

}
