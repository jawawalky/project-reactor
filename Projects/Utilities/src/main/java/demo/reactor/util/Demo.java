/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2012 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.reactor.util;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.swing.tree.DefaultMutableTreeNode;

/**
 * A helper class for demo programs.
 */
public final class Demo {

	// constants
	// ........................................................................

	/**
	 * A random number generator.
	 */
	private static final Random RANDOM = new Random();

	// fields
	// ........................................................................

	/**
	 * A counter for the log messages.
	 */
	private static int logCount = 1;

	// constructors
	// ........................................................................

	/**
	 * A private constructor.
	 */
	private Demo() {}

	// methods
	// ........................................................................
	
	/**
	 * Creates an error object, which signas some programming error.
	 * 
	 * @param cause the original cause of the error.
	 * 
	 * @return The error object.
	 */
	public static Error programmingError(final Exception cause) {
	    
	    throw new Error("Programming Error!", cause);
	    
	}

	/**
	 * Creates a runtime exception with a message, which can contain place
	 * holders.
	 * 
	 * @param message
	 *        the exception message.
	 *        
	 * @param args
	 *        the template arguments.
	 * 
	 * @return The runtime exception
	 */
	public static RuntimeException runtimeException(
		final String    message,
		final Object... args
	) {
	    
	    throw new RuntimeException(String.format(message, args));
	    
	}

	/**
	 * Waits for an input from the standard input stream, which by default is
	 * the console. The user can be prompted some text, specifying the input
	 * expected. If nothing is entered, then the default value will be
	 * returned.
	 * <p>
	 * <b>Note: </b> The program might block until the input was confirmed by
	 * pressing ENTER.
	 * 
	 * @param message the prompted text.
	 * @param defaultValue the value to be used, when nothing was entered.
	 * 
	 * @return The user input.
	 */
	public static synchronized String input(
		final String message,
		final String defaultValue
	) {
		
		String input = input(message);
		
		if ((input != null) && !("".equals(input))) {
			
			return input;
			
		} // if
		
		return defaultValue;

	}

	/**
	 * Waits for an input from the standard input stream, which by default is
	 * the console. The user can be prompted some text, specifying the input
	 * expected. If nothing is entered, then the default value will be
	 * returned.
	 * <p>
	 * <b>Note: </b> The program might block until the input was confirmed by
	 * pressing ENTER.
	 * 
	 * @param message the prompted text.
	 * @param defaultValue the value to be used, when nothing was entered.
	 * 
	 * @return The user input.
	 */
	public static synchronized int inputInt(
		final String message,
		final int    defaultValue
	) {
		
		String input = input(message);
		
		if ((input != null) && !("".equals(input))) {
			
			return Integer.parseInt(input);
			
		} // if
		
		return defaultValue;

	}

	/**
	 * Waits for an input from the standard input stream, which by default is
	 * the console. The user can be prompted some text, specifying the input
	 * expected. If nothing is entered, then the default value will be
	 * returned.
	 * <p>
	 * <b>Note: </b> The program might block until the input was confirmed by
	 * pressing ENTER.
	 * 
	 * @param message the prompted text.
	 * @param defaultValue the value to be used, when nothing was entered.
	 * 
	 * @return The user input.
	 */
	public static synchronized double inputDouble(
		final String message,
		final double defaultValue
	) {
		
		String input = input(message);
		
		if ((input != null) && !("".equals(input))) {
			
			return Double.parseDouble(input);
			
		} // if
		
		return defaultValue;

	}

	/**
	 * Waits for an input from the standard input stream, which by default is
	 * the console. The user can be prompted some text, specifying the input
	 * expected.
	 * <p>
	 * <b>Note: </b> The program might block until the input was confirmed by
	 * pressing ENTER.
	 * 
	 * @param message
	 *            the prompted text
	 * 
	 * @return The user input.
	 */
	public static synchronized String input(String message) {

		System.out.print(message);
		System.out.print("> ");

		BufferedReader br = null;
		String line = null;

		try {

			br = new BufferedReader(new InputStreamReader(System.in));
			line = br.readLine();

		} // try
		catch (Exception e) {

			terminate(e, -1);

		} // catch

		return line;

	}

	/**
	 * Writes a log messages to the output stream.
	 * 
	 * @param message
	 *        the message to be printed.
	 *        
	 * @param args
	 *        the template arguments.
	 */
	public static void log(
		final String    message,
		final Object... args
	) {

		synchronized (System.out) {
			
			String msg = String.format(message, args);
			
			System.out.printf("%-8s %s", "[" + logCount + "]", msg);
			System.out.println();
			System.out.println();

			logCount++;

		} // synchronized
		
	}

	/**
	 * Writes a log message about a raised exception. The displayed message
	 * contains the name of the exception class and the included error message.
	 * 
	 * @param cause
	 *        the exception or error.
	 */
	public static void log(final Throwable cause) {

		String line = "\n"
			+ "\t type :    "
			+ cause.getClass().getName()
			+ "\n"
			+ "\t message : "
			+ cause.getMessage();
		log(line);

		StringWriter sw = new StringWriter();
		cause.printStackTrace(new PrintWriter(sw));
		log(sw.toString());

	}
	
	/**
	 * Waits for a future result to complete and writes its value to
	 * the console. The waiting is done is a background thread,
	 * so this method will not block.
	 * 
	 * @param result
	 *        the future result.
	 */
	public static void log(final Future<?> result) {
		
		Runnable task = new Runnable() {
			
			@Override
			public void run() {
				
				try {
					
					Object value = result.get();
					log(value.toString());
					
				} // try
				catch (InterruptedException e) {
					
					// Do nothing.
					
				} // catch
				catch (ExecutionException e) {
					
					log(e);
					
				} // catch
				
			}
			
		};
		
		Thread thread = new Thread(task);
		thread.setDaemon(true);
		thread.start(); 
		
	}

	/**
	 * Writes a log messages to the output stream, adding the thread ID.
	 *
	 * @param message
	 *        the message to be printed.
	 *
	 * @param args
	 *        the template arguments.
	 */
	public static void logWithThread(
			final String    message,
			final Object... args
	) {

		log(
			"[%d:%s] %s",
			Thread.currentThread().getId(),
			Thread.currentThread().getName(),
			String.format(message, args)
		);

	}

	/**
	 * Writes a string to the output stream. It adds a new line sign at the end.
	 * 
	 * @param text the text to be printed on the output stream.
	 */
	public static void println(final String text) {
		
		synchronized (System.out) {
			
			System.out.println(text);
			
		} // synchronized

	}

	/**
	 * Writes a new line sign at the output stream.
	 */
	public static void println() {
		
		synchronized (System.out) {
			
			System.out.println();
			
		} // synchronized

	}

	/**
	 * Writes n new lines.
	 * 
	 * @param n
	 *        the number of new lines.
	 */
	public static void println(final int n) {
		
		synchronized (System.out) {
			
			for (int i = 0; i < n; i++) {
				
				println();
				
			} // for
			
		} // synchronized

	}

	/**
	 * Writes a string to the output stream.
	 * 
	 * @param text
	 *        the text to be printed on the output stream.
	 */
	public static void print(final String text) {

		synchronized (System.out) {
			
			System.out.print(text);
			
		} // synchronized

	}
	
	/**
	 * Prints a caption text.
	 */
	public static void printCaption(final String text) {
		
		synchronized (System.out) {
			
			final String LINE = "> ------------------------------------------------";
			println(LINE);
			println("> - " + text);
			println(LINE);
			
		} // synchronized
	
	}
	
	/**
	 * Prints the name of the method, in which this method was called.
	 */
	public static void printNameOfMethod() {
		
		printCaption(getNameOfMethod());
		
	}
	
	/**
	 * Returns the name of the method, in which this method was called.
	 * 
	 * @return The method name.
	 */
	public static synchronized String getNameOfMethod() {
		
		return Thread.currentThread().getStackTrace()[2].getMethodName();
		
	}
			
	/**
	 * Terminates the program without a message and the given exit code.
	 * 
	 * @param code the exit code
	 */
	public static void terminate(final int code) {

		log("Program terminated!");
		System.exit(code);

	}

	/**
	 * Terminates the program after an exception has occurred, returning the
	 * given exit code.
	 * 
	 * @param exception the exception
	 * @param code the exit code
	 */
	public static void terminate(
		final Exception exception,
		final int       code
	) {

		log(exception);
		terminate(code);

	}
	
	/**
	 * Create a comma-separated list of all values of the collection.
	 * 
	 * @param collection
	 *        the collection.
	 *        
	 * @param delimiter
	 *        the delimiter character sequence. <i>required</i>
	 *        
	 * @return The comma-separated value list.
	 */
	public static String csv(
		final Collection<?> collection,
		final String        delimiter
	) {
		
		requireNonNull(
			delimiter,
			"The delimiter must not be null!"
		);
		
		return
			(collection != null) ?
			collection.stream().map(e -> e.toString()).collect(joining(delimiter)) :
			"";
		
	}
	
	/**
	 * Create a comma-separated list of all values of the collection.
	 * 
	 * @param collection
	 *        the collection.
	 *        
	 * @return The comma-separated value list.
	 */
	public static String csv(final Collection<?> collection) {
		
		return csv(collection, ", ");
		
	}
	
	/**
	 * Keeps the current thread alive.
	 */
	public static void keepAlive() {
		
		log(Thread.currentThread().getName() + " is sleeping for 5 minutes ...");
		sleep(5 * 60 * 1000);
		
	}
	
	/**
	 * Prints the exception of a failed task.
	 * 
	 * @param task
	 *        the failable task.
	 */
	public static void handled(final Failable task) {
		
		try { task.run(); } // try
		catch (Throwable e) { Demo.log(e); } // catch
		
	}

	/**
	 * Returns the value of a property, which is either set in the file
	 * {@code demo.properties} or in the system properties.
	 * 
	 * @param key the property key.
	 * 
	 * @return The property value.<br>
	 * 		{@code null}, if the property is not defined.
	 */
	public static String getProperty(final String key) {
		
		String value = null;
		
		// A) Load a property from a file called
		//    'demo.properties' located in the current
		//    working directory.
		//
		Properties      props = new Properties();
		FileInputStream in    = null;
		
		try {
			
			in = new FileInputStream("demo.properties");
			props.load(in);
			value = props.getProperty(key);
			
		} // try
		catch (Exception ex) {
		} // catch
		finally {
			
			try {in.close();}		catch (Exception ex) {}
			
		} // finally
		
		if (value != null) {
			
			return value;
			
		} // if
		
		
		// B) Read the value from the system properties.
		//
		return System.getProperty(key);
		
	}
    
    /**
	 * Returns the value of a property, which is either set in the file
	 * {@code demo.properties} or in the system properties.
     * If no value can be retrieved, we return the specified default
     * value.
     * 
     * @param key the property key.
     * @param defaultValue the default value.
     * 
     * @return The property value.
     * 
     * @see #getProperty(String)
     */
    public static String getProperty(
    	final String key,
    	final String defaultValue
    ) {
        
        String value = getProperty(key);
        
        if (value == null) {
            
            value = defaultValue;
            
        } // if
        
        return value;
        
    }

	/**
	 * Lets the current thread sleep for the specified time.
	 * 
	 * @param millis the time in milliseconds.
	 */
	public static void sleep(final long millis) {
		
		try {
			
			Thread.sleep(millis);
			
		} // try
		catch (InterruptedException e) {
			
			// Do nothing.
			
		} // catch
		
	}

	/**
	 * Lets the current thread sleep for some random time between the
	 * minimum and the maximum sleep time.
	 * 
	 * @param minTime the minimum sleep time in milliseconds.
	 * @param minTime the maximum sleep time in milliseconds.
	 */
	public static void sleep(final long minTime, final long maxTime) {
		
		sleep(minTime + RANDOM.nextInt((int) (maxTime - minTime)));
		
	}
	
	/**
	 * Returns a random <code>int</code> value in the interval [0, n).
	 * 
	 * @param n the upper limit of the interval.
	 * 
	 * @return The random value.
	 */
	public static int nextInt(final int n) {
		
		return RANDOM.nextInt(n);
		
	}

	/**
	 * Writes a byte array with its length to an output stream.
	 * 
	 * @param out the output stream.
	 * @param data the byte array.
	 */
	public static void writeByteArray(final OutputStream out, final byte[] data)
		throws IOException
	{
	    
	    DataOutputStream dout = new DataOutputStream(out);
	    
	    if (data != null) {
	        
	        dout.writeInt(data.length);
	        dout.write(data);
	        
	    }
	    else {
            
            dout.writeInt(-1);
            
	    } // if
        
	}

    /**
     * Writes an <code>int</code> value.
     * 
     * @param out the output stream.
     * @param value the byte array.
     */
    public static void writeInt(final OutputStream out, final int value)
        throws IOException
    {
        
        DataOutputStream dout = new DataOutputStream(out);
        dout.writeInt(value);
        
    }

	/**
	 * Reads a byte array from an input stream. The array must have been
	 * written with the {@link #writeByteArray(OutputStream, byte[])}
	 * method.
	 * 
	 * @param in the input stream.
	 * 
	 * @return The byte array.
	 */
	public static byte[] readByteArray(final InputStream in)
		throws IOException
	{
	    
	    DataInputStream din = new DataInputStream(in);
	    
	    int    length = din.readInt();
        byte[] data   = null;
	    
	    if (length != -1) {
	        
	        data = new byte[length];
	        din.read(data);
	        
	    } // if
	    
	    return data;
				
	}

    /**
     * Reads an <code>int</code> value.
     * 
     * @param in the input stream.
     * 
     * @return The {@code int} value.
     */
    public static int readInt(final InputStream in) throws IOException {
        
        DataInputStream din = new DataInputStream(in);
        return din.readInt();
                
    }
    
	/**
	 * Loads the file content into memory as a <code>byte</code> array.
	 * 
	 * @param fileName the file name.
	 * 
	 * @return The file content.
	 */
	public static byte[] loadFile(final String fileName) {
		
		InputStream in = null;
		
		try {
			
			in = new BufferedInputStream(new FileInputStream(fileName));
			int size = in.available();
			byte[] data = new byte[size];
			in.read(data);
			return data;
			
		} // try
		catch (Exception e) {
			
			throw new Error(e);
			
		} // catch
		finally {
			
			try { in.close(); }		catch (Exception e) {}
			
		} // finally
		
	}

	/**
	 * Returns a string which contains the <code>byte</code> array information
	 * in hexadecimal enoding.
	 * 
	 * @param array the <code>byte</code> array.
	 * 
	 * @return The string.
	 */
	public static String toHex(byte[] array) {
		
		if (array == null) {
			
			return null;
			
		} // if
		
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0; i < array.length; i++) {
			
			int value = (int)array[i] - (int)Byte.MIN_VALUE;
			String s = Integer.toHexString(value);
			
			if(s.length() < 2) {
				
				s = "0" + s;
				
			} // if
			
			sb.append(s);
			
			if (i < array.length - 1) {
				
				sb.append(":");
				
			} // if
			
		} // for
		
		return sb.toString();
		
	}
	
	/**
	 * Closes some open stream.
	 * 
	 * @param stream the stream to be closed.
	 */
	public static void close(final Closeable stream) {
	    
	    try {
	        
            stream.close();
            
        } // try
        catch (IOException e) {
            
            // Do nothing.
            
        } // catch
	    
	}
	
	public static DefaultMutableTreeNode createNode(final Class<?> type) {
	    
	    DefaultMutableTreeNode node = new DefaultMutableTreeNode(type);
	    
	    for (Annotation annotation : type.getAnnotations()) {
	        
	        node.add(createNode(annotation));
	        
	    } // for
	    
	    for (Field field : type.getFields()) {
	        
	        node.add(createNode(field));
	        
	    } // for
	    
        for (Constructor<?> constructor : type.getConstructors()) {
            
            node.add(createNode(constructor));
            
        } // for
        
        for (Method method : type.getMethods()) {
            
            node.add(createNode(method));
            
        } // for
        
	    return node;
	    
	}
	
    public static DefaultMutableTreeNode createNode(final Annotation annotation) {
        
        return new DefaultMutableTreeNode(annotation);
        
    }
    
    public static DefaultMutableTreeNode createNode(final Field field) {
        
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(field);
        
        for (Annotation annotation : field.getAnnotations()) {
            
            node.add(createNode(annotation));
            
        } // for
        
        return node;
        
    }
    
    public static DefaultMutableTreeNode createNode(final Constructor<?> constructor) {
        
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(constructor);
        
        for (Annotation annotation : constructor.getAnnotations()) {
            
            node.add(createNode(annotation));
            
        } // for
        
        return node;
        
    }
    
    public static DefaultMutableTreeNode createNode(final Method method) {
        
        DefaultMutableTreeNode node = new DefaultMutableTreeNode(method);
        
        for (Annotation annotation : method.getAnnotations()) {
            
            node.add(createNode(annotation));
            
        } // for
        
        return node;
        
    }
    
	/**
	 * Encodes a string, so it only contains characters, which are allowed in
	 * URLs. We perform the following replacements
	 * <ul>
	 * 	<li><i>blank</i> --&gt; <code>%20</code></li>
	 * </ul>
	 * 
	 * @param value the string value.
	 * 
	 * @return The encoded value.
	 */
	public static String encode(String value) {
		
		return value.replace(" ", "%20");
		
	}

	/**
	 * Decodes a string, so encoded characters are replaced by their original
	 * characters. We perform the following replacements
	 * <ul>
	 * 	<li><code>%20</code> --&gt; <i>blank</i></li>
	 * </ul>
	 * 
	 * @param value the string value.
	 * 
	 * @return The decoded value.
	 */
	public static String decode(String value) {
		
		return value.replace("%20", " ");
		
	}
	
	/**
	 * Says, if an object is {@code null}.
	 * 
	 * @param obj
	 *        the object.
	 *        
	 * @return {@code true}, if the object is {@code null}.<br>
	 *         {@code false}, if not.
	 */
	public static boolean isNull(final Object obj) {
		
		return obj == null;
		
	}
	
	/**
	 * Says, if an object is not {@code null}.
	 * 
	 * @param obj
	 *        the object.
	 *        
	 * @return {@code true}, if the object is not {@code null}.<br>
	 *         {@code false}, if not.
	 */
	public static boolean isNotNull(final Object obj) {
		
		return !isNull(obj);
		
	}
	
	/**
	 * Says, if a string is empty, i.e. it has length 0.
	 * 
	 * @param s
	 *        the string.
	 *        
	 * @return {@code true}, if the string is empty.<br>
	 *         {@code false}, if not.
	 */
	public static boolean isEmpty(final String s) {
		
		return s.isEmpty();
		
	}
	
	/**
	 * Says, if a string is null or empty.
	 * 
	 * @param s
	 *        the string.
	 *        
	 * @return {@code true}, if the string is {@code null} or empty.<br>
	 *         {@code false}, if not.
	 *         
	 * @see #isNull(Object)
	 * @see #isEmpty(String)
	 */
	public static boolean isNullOrEmpty(final String s) {
		
		return isNull(s) || isEmpty(s);
		
	}
	
	/**
	 * Says, if a string is not empty, i.e. it must not be {@code null} and
	 * its length must be greater than 0.
	 * <p/>
	 * <b>Note:</b>
	 * <blockquote>
	 * 	A non-empty string must contain at least one character. But all its
	 * 	character can be white-space characters. If you want the at least one
	 * 	non-white-space character to be contained in the string, then use
	 * 	the method {@link #isNonTrivial(String)}.
	 * </blockquote>
	 * 
	 * @param s
	 *        the string.
	 *        
	 * @return {@code true}, if the string is not empty.<br>
	 *         {@code false}, if not.
	 *         
	 * @see #isNullOrEmpty(String)
	 * @see #isNonTrivial(String)
	 */
	public static boolean isNotEmpty(final String s) {
		
		return !isNullOrEmpty(s);
		
	}
	
	/**
	 * Says, if a string is
	 * <ul>
	 * 	<li>{@code null},</li>
	 * 	<li>empty or</li>
	 * 	<li>consists only of white-spaces</li>
	 * </ul>
	 * 
	 * @param s
	 *        the string.
	 *        
	 * @return {@code true}, if the string is trivial.<br>
	 *         {@code false}, if not.
	 *         
	 * @see #isNull(Object)
	 * @see #isEmpty(String)
	 */
	public static boolean isTrivial(final String s) {
		
		return isNull(s) || isEmpty(s.trim());
		
	}
	
	/**
	 * Says, if a string is
	 * <ul>
	 * 	<li>not {@code null},</li>
	 * 	<li>not empty and</li>
	 * 	<li>does not only consist of whitespaces</li>
	 * </ul>
	 * 
	 * @param s
	 *        the string.
	 *        
	 * @return {@code true}, if the string is non-trivial.<br>
	 *         {@code false}, if not.
	 *         
	 * @see #isTrivial(String)
	 */
	public static boolean isNonTrivial(final String s) {
		
		return !isTrivial(s);
		
	}
	
	/**
	 * Checks, if a string contains white-space characters.
	 * 
	 * @param s
	 *        the string.
	 *        
	 * @return {@code true}, if the string contains white-spaces.<br>
	 *         {@code false}, if not.
	 */
	public static boolean containsWhitespaces(final String s) {
		
		if (s == null)									return false;
		if (s.isEmpty())								return false;
		
		for (int i = 0; i < s.length(); i++) {
			
			if (Character.isWhitespace(s.charAt(i)))	return true;
			
		} // for
		
		return false;
		
	}
	
	
	// Date & Time Operations
	
	/**
	 * Returns the current date on this system.
	 * 
	 * @return The current date.
	 */
	public static LocalDate localDate() {
		
		return LocalDate.now();
		
	}
	
	/**
	 * Returns the current time on this system.
	 * 
	 * @return The current time.
	 */
	public static LocalTime localTime() {
		
		return LocalTime.now();
		
	}
	
	/**
	 * Converts a {@code LocalDate} object into a {@code Date} object.
	 * 
	 * @param localDate
	 *        the {@code LocalDate} object.
	 *        
	 * @return The {@code Date} object.
	 */
	public static Date toDate(final LocalDate localDate) {
		
		return
			Date.from(
				localDate.atStartOfDay(ZoneId.systemDefault())
				.toInstant()
			);
		
	}
	
	/**
	 * Converts a {@code LocalTime} object into a {@code Date} object.
	 * 
	 * @param localTime
	 *        the {@code LocalTime} object.
	 *        
	 * @return The {@code Date} object.
	 */
	public static Date toDate(final LocalTime localTime) {
		
		return
			Date.from(
				localTime
					.atDate(LocalDate.now())
					.atZone(ZoneId.systemDefault())
					.toInstant()
			);
		
	}
	
	/**
	 * Converts a {@code Date} object into a {@code LocalDate} object.
	 * 
	 * @param date
	 *        the {@code Date} object.
	 *        
	 * @return The {@code LocalDate} object.
	 */
	public static LocalDate toLocalDate(final Date date) {
		
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		
	}
	
	/**
	 * Converts a {@code Date} object into a {@code LocalTime} object.
	 * 
	 * @param date
	 *        the {@code Date} object.
	 *        
	 * @return The {@code LocalTime} object.
	 */
	public static LocalTime toLocalTime(final Date date) {
		
		return date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
		
	}
	
	/**
	 * Returns a string in the format <i>yyyy-MM-dd</i>.
	 * 
	 * @param date
	 *        the date to be formatted.
	 *        
	 * @return The date string.
	 */
	public static String formatDate(final Date date) {
		
		return toLocalDate(date).toString();
		
	}
	
	/**
	 * Returns a {@code Date} by parsing a string with the format
	 * <i>yyyy-MM-dd</i>.
	 * 
	 * @param dateString
	 *        the string to be parsed.
	 *        
	 * @return The date.
	 */
	public static Date parseDate(final String dateString) {
		
		return toDate(LocalDate.parse(dateString));
		
	}
	
	/**
	 * The local date of today.
	 * 
	 * @return The date of today.
	 */
	public LocalDate today() { return LocalDate.now(); }
	
	/**
	 * The local date of tomorrow.
	 * 
	 * @return The date of tomorrow.
	 */
	public LocalDate tomorrow() { return today().plusDays(1); }
	
	/**
	 * The local date of yesterday.
	 * 
	 * @return The date of yesterday.
	 */
	public LocalDate yesterday() { return today().minusDays(1); }
	
}
