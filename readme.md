# Project Reactor

In this training you will learn about the foundations of the *Spring Framework*, such as

- Dependency Injection
- Aspect-oriented Programming (AOP)
- Data Access
- Integration
- etc.

All demos and exercises are provided as *Maven* projects, which can usually be imported into any IDE, such as *Eclipse*, *IntelliJ*, etc.

## Setup

If you want to build and run the following projects, then you need

- [*JDK 17*](https://jdk.java.net/archive/) or higher
- [*Maven*](https://maven.apache.org/download.cgi)
- [*GIT*](https://git-scm.com/downloads)
- [*Eclipse*](https://www.eclipse.org/downloads/) or some other IDE

Install all those programs on your computer and make sure all programs except your IDE are added to the program path and can be called without path in any directory of a terminal window.

**Example**

- Open a terminal window.
- Type `git --version` and check, if the program can be found and prints something like `git version 2.30.2` on the console (of course the version number may be different).

**Checking Versions**

You can check the versions of the installed programs by

- *GIT*: `git --version`
- *Java*: `java -version`
- *Maven*: `mvn -v`

## Programs

What is the purpose of the different programs?

### JDK

The *JDK (Java Development Kit)* is the development environment for *Java* applications.

### GIT

*GIT* is a version control system, which allows us to store different states of our development. The location, where *GIT* stores the data about a project is called a *repository*.

All demos and execises are stored in a *GIT* repository on *gitlab.com*. You can clone this repository on your local computer.

### Maven

*Maven* is a build tool that allows us to automate the build of our *Java* projects.

It can download required libraries and add them to the class- or module-path of our project.

> If you are behind a proxy, you may need to set the proxy settings for *Maven* to work properly.

## Building Projects

In this training you are going to work on different exercises. Before you can start, you need to clone them on your own computer.

### Cloning the GIT Repository

With *GIT* installed you can clone the repository

1. Open a terminal window.
2. Go to the folder, where you want to store the clone of the repository.
3. Run the command `git clone https://gitlab.com/jawawalky/spring-framework.git`

The folder, where you cloned the project, should contain a sub-folder called *spring-framework*. It contains all demos and exercises.

### Building a Single Project

If you want to build a single project, then do the following

1. Open a terminal window.
2. Go to the project folder.
3. Run `mvn install`.

When *Maven* has finished and you see something like

```
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  0.820 s
[INFO] Finished at: 2021-09-13T09:20:25+02:00
[INFO] ------------------------------------------------------------------------
```

then the build was successful.

> *Maven* downloads all required libraries from the Internet from a location called *Maven Central*.
> If you have problems connecting to *Maven Central*, then these problems are usually related to a proxy denying direct access to the Internet.
> You can try to set the environment variables *HTTP_PROXY* and *HTTPS_PROXY*.
> Or you can try to configure the [proxy settings of *Maven*](https://maven.apache.org/guides/mini/guide-proxies.html). 

#### Priority

There are some dependencies between the projects. All projects use the project **Utilities**. So before you build other projects, you should build *Utilities* first.

When you run projects, where a *JMS* server is needed, then you need to build the project *ArtemisMQ*.

> You find all projects in the folder *spring-framework/Projects*.

### Building all Projects

On *Linux* or on terminals, which can execute *Unix* scripts, you can build all projects by running the script `./build-all.sh` in a terminal window.

> You find all scripts in the folder *spring-framework/Scripts*.

## Adding a Project to your IDE

### Eclipse

You can add a project to *Eclipse*.

1. Start *Eclipse*.
2. Go to the *Package Explorer*.
3. Right click and choose `Import ... > Existing Maven Projects`.
4. Browse and select the desired project.
5. Open `Advanced` and select `[name]` as `Name template`.
6. Press `Finish`.
